import { Outlet, RouterProvider, createBrowserRouter } from 'react-router-dom';
import './App.css';
import Body from './Components/Body';
import SignUp from './Components/SingUp';
import Login from './Components/Login';
import { Provider } from 'react-redux';
import store from './store/store';

const appRouter = createBrowserRouter([
  {
    path: "/dashboard",
    element: <Body />,
  },
  {
    path: "/signUp",
    element: <SignUp />,
  }, {
    path: "/",
    element: <Login />
  }
])   //* click in the profile section to go to login page

function App() {
  return (
    <>
        <Provider store = {store}>
        <RouterProvider router={appRouter} />
        </Provider>
    </>
  );
}

export default App;
