import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { addUser } from "../store/UserSlice";
import { loggin } from "../store/LoginSlice";
import store from "../store/store";
import Popup from "./Popup";

const SignUp = () => {
  const dispatch = useDispatch();

  // const [submit, setSubmit] = useState(false);

  const [showPopup, setShowPopup] = useState(false);

  const navigate = useNavigate();

  // useEffect(()=> {
  const isLoggedIn = useSelector((store) => store.isLoggedIn);
  // console.log(isLoggedIn);
  // }, [submit])

  const [formData, setFormData] = useState({
    name: "",
    email: "",
    phoneNumber: "",
    password: "",
    confirmPassword: "",
  });

  const [errors, setErrors] = useState({
    name: "",
    email: "",
    phoneNumber: "",
    password: "",
    confirmPassword: "",
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const timestamp = Date.now();
  const key = `orderedItemsInStore_${timestamp}`;

  const handleSubmit = (e) => {
    e.preventDefault();
    if (validateForm()) {
      console.log(formData);
      dispatch(addUser(formData));
      dispatch(loggin(true));
      setShowPopup(true);
      window.localStorage.setItem(key, JSON.stringify(formData));
      setTimeout(() => {
        setShowPopup(false);
        navigate("/dashboard");
      }, 2000);
    }
  };

  const validateForm = () => {
    let isValid = true;
    const newErrors = {
      name: "",
      email: "",
      phoneNumber: "",
      password: "",
      confirmPassword: "",
    };

    const nameRegex = /^[a-zA-Z\s]+$/;
    if (!formData.name.trim() || !nameRegex.test(formData.name)) {
      newErrors.name = "Please enter a valid name.";
      isValid = false;
    }

    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!formData.email || !emailRegex.test(formData.email)) {
      newErrors.email = "Please enter a valid email.";
      isValid = false;
    }

    const phoneRegex = /^\d{10}$/;
    if (!formData.phoneNumber || !phoneRegex.test(formData.phoneNumber)) {
      newErrors.phoneNumber = "Please enter a valid phone number.";
      isValid = false;
    }

    if (!formData.password || formData.password.length < 6) {
      newErrors.password =
        "Please enter a password with at least 6 characters.";
      isValid = false;
    }

    if (formData.password !== formData.confirmPassword) {
      newErrors.confirmPassword = "Passwords do not match.";
      isValid = false;
    }

    setErrors(newErrors);
    return isValid;
  };

  return (
    <div className="signUp">
      <img className="signUpLogo" src="/Rexav Logo.png" alt="logo" />
      <h2 className="signUpH2">Sign up for a Rexav Account</h2>
      <form onSubmit={handleSubmit} className="signUpForm">
        <label>
          What's your name?
          <input
            type="text"
            name="name"
            placeholder="Full Name"
            required
            value={formData.name}
            onChange={handleChange}
          />
          {errors.name && <span className="error">{errors.name}</span>}
        </label>
        <label>
          What's your email?
          <input
            type="email"
            name="email"
            placeholder="Email"
            required
            value={formData.email}
            onChange={handleChange}
          />
          {errors.email && <span className="error">{errors.email}</span>}
        </label>
        <label>
          What's your phone number?
          <input
            type="tel"
            name="phoneNumber"
            placeholder="Phone Number"
            required
            value={formData.phoneNumber}
            onChange={handleChange}
          />
          {errors.phoneNumber && (
            <span className="error">{errors.phoneNumber}</span>
          )}
        </label>
        <label>
          Create password
          <input
            type="password"
            name="password"
            placeholder="Password"
            required
            value={formData.password}
            onChange={handleChange}
          />
          {errors.password && <span className="error">{errors.password}</span>}
        </label>
        <label>
          Confirm password
          <input
            type="password"
            name="confirmPassword"
            placeholder="Confirm Password"
            required
            value={formData.confirmPassword}
            onChange={handleChange}
          />
          {errors.confirmPassword && (
            <span className="error">{errors.confirmPassword}</span>
          )}
        </label>
        <button type="submit" className="signUpSubmitButton">
          Sign up
        </button>
      </form>
      <div className="loginWrittingSection">
        <p className="loginWritting">Already have an account ?</p>
        <Link to={"/"} className="loginp">
          Login.
        </Link>
      </div>

      {isLoggedIn.status && showPopup && (
        <Popup content={"Account Created succesfully"} />
      )}
    </div>
  );
};

export default SignUp;
