import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { loggin } from "../store/LoginSlice";
import Popup from "./Popup";
import { addUser } from "../store/UserSlice";

const Login = () => {
  const [formData, setFormData] = useState({
    email: "",
    password: "",
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  // const user = JSON.parse(window.localStorage.getItem("user"));
  // console.log(user);

  const [users, setUsers] = useState([])
  // console.log(users);

  useEffect(() => {
    const usersFromLoaclStorage = [];

    const totalItems = localStorage.length;
    for (let i = 0; i < totalItems; i++) {
      const key = localStorage.key(i);
      const data = localStorage.getItem(key);
      const users = JSON.parse(data);
      // console.log(users);
      usersFromLoaclStorage.push(users);
    }

    setUsers(usersFromLoaclStorage);
  }, []);

  const [showPopup, setShowPopup] = useState(false);

  const [popupContent, setPopupContent] = useState("");

  const navigate = useNavigate();

  const dispatch = useDispatch();

  // const user = useSelector((store)=> store.user.users[0])
  // console.log(user)

  const verifyUser = () => {
    const existingUser = users.find((user)=> user.email === formData.email && user.password === formData.password)
    // console.log(existingUser)
    if (existingUser) {
      dispatch(loggin(true));
      setShowPopup(true);
      setPopupContent("Login successfull");
      dispatch(addUser(existingUser))
    } else {
      setPopupContent("Invalid Email or Password");
      setShowPopup(true);
    }
  };

  const isLoggedIn = useSelector((store) => store.isLoggedIn);
  // console.log(isLoggedIn);

  useEffect(() => {
    if (isLoggedIn.status) {
      const timeoutId = setTimeout(() => {
        navigate("/dashboard");
      }, 2000);

      return () => clearTimeout(timeoutId);
    }
  }, [isLoggedIn.status, navigate]);

  const handleSubmit = (e) => {
    e.preventDefault();
    // console.log(formData);
    verifyUser();
    setTimeout(() => {
      setShowPopup(false);
    }, 2000);
  };

  return (
    <div className="signUp">
      <img className="signUpLogo" src="/Rexav Logo.png" alt="logo" />
      <h2 className="signUpH2">Login to a Rexav Dashboard</h2>
      <form onSubmit={handleSubmit} className="signUpForm">
        <label>
          Email:
          <input
            type="email"
            name="email"
            placeholder="Email"
            required
            value={formData.email}
            onChange={handleChange}
          />
        </label>
        <label>
          Password:
          <input
            type="password"
            name="password"
            placeholder="Password"
            required
            value={formData.password}
            onChange={handleChange}
          />
        </label>
        <button type="submit" className="signUpSubmitButton">
          Login
        </button>
      </form>
      <div className="loginWrittingSection">
        <p className="loginWritting">Don't have an account ?</p>
        <Link to={"/signUp"} className="loginp">
          Sign up for Rexav
        </Link>
      </div>
      {showPopup && <Popup content={popupContent} />}
    </div>
  );
};

export default Login;
