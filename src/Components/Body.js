import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import store from "../store/store";

function Body() {
  const user = useSelector((store) => store.user.users[0]);
  console.log(user);

  const userToDisplay = JSON.parse(window.localStorage.getItem("user"));
  console.log(userToDisplay);

  const navigate = useNavigate();

  const handleProfileButton = () => {};

  useEffect(() => {
    if (user) {
      window.localStorage.setItem("user", JSON.stringify(user));
    }
  }, []);

  const [task, setTask] = useState(false);
  const [project, setProject] = useState(false);
  const [team, setTeam] = useState(false);
  const [start, setStart] = useState(false);
  const [theme, setTheme] = useState(false);

  return (
    <>
      <div className="body">
        <div className="sideBar">
          <div className="sideBarsection1">
            <div className="sideBarIcons">
              <img className="logo" src="/Rexav Logo.png" alt="logo" />

              <svg
                className="sideBarIcons2"
                width="33"
                height="33"
                viewBox="0 0 33 33"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M12.6357 4.97137H5.96908C5.2327 4.97137 4.63574 5.56833 4.63574 6.30471V12.9714C4.63574 13.7078 5.2327 14.3047 5.96908 14.3047H12.6357C13.3721 14.3047 13.9691 13.7078 13.9691 12.9714V6.30471C13.9691 5.56833 13.3721 4.97137 12.6357 4.97137Z"
                  stroke="white"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
                <path
                  d="M27.3025 4.97137H20.6358C19.8994 4.97137 19.3025 5.56833 19.3025 6.30471V12.9714C19.3025 13.7078 19.8994 14.3047 20.6358 14.3047H27.3025C28.0389 14.3047 28.6358 13.7078 28.6358 12.9714V6.30471C28.6358 5.56833 28.0389 4.97137 27.3025 4.97137Z"
                  stroke="white"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
                <path
                  d="M27.3025 19.638H20.6358C19.8994 19.638 19.3025 20.235 19.3025 20.9714V27.638C19.3025 28.3744 19.8994 28.9714 20.6358 28.9714H27.3025C28.0389 28.9714 28.6358 28.3744 28.6358 27.638V20.9714C28.6358 20.235 28.0389 19.638 27.3025 19.638Z"
                  stroke="white"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
                <path
                  d="M12.6357 19.638H5.96908C5.2327 19.638 4.63574 20.235 4.63574 20.9714V27.638C4.63574 28.3744 5.2327 28.9714 5.96908 28.9714H12.6357C13.3721 28.9714 13.9691 28.3744 13.9691 27.638V20.9714C13.9691 20.235 13.3721 19.638 12.6357 19.638Z"
                  stroke="white"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
              </svg>

              <svg
                className="sideBarIcon"
                width="32"
                height="32"
                viewBox="0 0 32 32"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M21.3334 28V25.3333C21.3334 23.9188 20.7715 22.5623 19.7713 21.5621C18.7711 20.5619 17.4146 20 16.0001 20H8.00008C6.58559 20 5.22904 20.5619 4.22885 21.5621C3.22865 22.5623 2.66675 23.9188 2.66675 25.3333V28"
                  stroke="black"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
                <path
                  d="M12.0001 14.6667C14.9456 14.6667 17.3334 12.2789 17.3334 9.33333C17.3334 6.38781 14.9456 4 12.0001 4C9.05456 4 6.66675 6.38781 6.66675 9.33333C6.66675 12.2789 9.05456 14.6667 12.0001 14.6667Z"
                  stroke="black"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
                <path
                  d="M29.3333 28V25.3333C29.3324 24.1516 28.9391 23.0037 28.2151 22.0698C27.4911 21.1358 26.4774 20.4688 25.3333 20.1733"
                  stroke="black"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
                <path
                  d="M21.3333 4.17334C22.4805 4.46707 23.4973 5.13427 24.2234 6.06975C24.9496 7.00523 25.3437 8.15578 25.3437 9.34001C25.3437 10.5242 24.9496 11.6748 24.2234 12.6103C23.4973 13.5457 22.4805 14.2129 21.3333 14.5067"
                  stroke="black"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
              </svg>

              <svg
                className="sideBarIcon"
                width="32"
                height="32"
                viewBox="0 0 32 32"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M16 17.3333V2.66667L26.6667 8L16 13.3333"
                  stroke="black"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
                <path
                  d="M27.4001 13.64C28.1096 15.8269 28.176 18.1715 27.5913 20.395C27.0067 22.6185 25.7956 24.6272 24.1022 26.1822C22.4087 27.7373 20.3042 28.7731 18.0391 29.1664C15.7739 29.5598 13.4434 29.2942 11.3248 28.4012C9.20626 27.5082 7.38882 26.0254 6.08873 24.1292C4.78864 22.233 4.06062 20.0033 3.9913 17.7052C3.92197 15.4072 4.51426 13.1376 5.69769 11.1665C6.88111 9.19541 8.60587 7.60575 10.6667 6.58667"
                  stroke="black"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
                <path
                  d="M10.6666 13.3333C9.99846 14.2227 9.5635 15.2651 9.40132 16.3656C9.23914 17.4661 9.3549 18.5896 9.73806 19.6339C10.1212 20.6782 10.7596 21.61 11.595 22.3445C12.4305 23.0789 13.4364 23.5927 14.5212 23.8389C15.6059 24.0851 16.7351 24.0559 17.8057 23.7541C18.8764 23.4523 19.8544 22.8873 20.6509 22.1108C21.4473 21.3342 22.0368 20.3707 22.3656 19.308C22.6943 18.2454 22.752 17.1173 22.5333 16.0267"
                  stroke="black"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
              </svg>

              <svg
                className="sideBarIcon"
                width="32"
                height="32"
                viewBox="0 0 32 32"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M26.6667 9.33334H5.33341C3.86066 9.33334 2.66675 10.5273 2.66675 12V25.3333C2.66675 26.8061 3.86066 28 5.33341 28H26.6667C28.1395 28 29.3334 26.8061 29.3334 25.3333V12C29.3334 10.5273 28.1395 9.33334 26.6667 9.33334Z"
                  stroke="black"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
                <path
                  d="M21.3334 28V6.66667C21.3334 5.95942 21.0525 5.28115 20.5524 4.78105C20.0523 4.28095 19.374 4 18.6667 4H13.3334C12.6262 4 11.9479 4.28095 11.4478 4.78105C10.9477 5.28115 10.6667 5.95942 10.6667 6.66667V28"
                  stroke="black"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
              </svg>

              <svg
                className="sideBarIcon"
                width="32"
                height="32"
                viewBox="0 0 32 32"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M10.6667 28H26.6667C27.374 28 28.0523 27.719 28.5524 27.219C29.0525 26.7189 29.3334 26.0406 29.3334 25.3333V22.6667H13.3334V25.3333C13.3334 26.0406 13.0525 26.7189 12.5524 27.219C12.0523 27.719 11.374 28 10.6667 28ZM10.6667 28C9.9595 28 9.28123 27.719 8.78113 27.219C8.28103 26.7189 8.00008 26.0406 8.00008 25.3333V6.66667C8.00008 5.95942 7.71913 5.28115 7.21903 4.78105C6.71894 4.28095 6.04066 4 5.33341 4C4.62617 4 3.94789 4.28095 3.4478 4.78105C2.9477 5.28115 2.66675 5.95942 2.66675 6.66667V10.6667H8.00008"
                  stroke="black"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
                <path
                  d="M25.3333 22.6667V6.66667C25.3333 5.95942 25.0523 5.28115 24.5522 4.78105C24.0521 4.28095 23.3738 4 22.6666 4H5.33325"
                  stroke="black"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
                <path
                  d="M19.9999 10.6667H13.3333"
                  stroke="black"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
                <path
                  d="M19.9999 16H13.3333"
                  stroke="black"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
              </svg>

              <svg
                className="sideBarIcon"
                width="32"
                height="32"
                viewBox="0 0 32 32"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M22.6667 3.99999C23.0169 3.64979 23.4327 3.37201 23.8902 3.18248C24.3478 2.99296 24.8382 2.89542 25.3334 2.89542C25.8287 2.89542 26.3191 2.99296 26.7766 3.18248C27.2342 3.37201 27.6499 3.64979 28.0001 3.99999C28.3503 4.35018 28.6281 4.76592 28.8176 5.22346C29.0071 5.68101 29.1047 6.17141 29.1047 6.66665C29.1047 7.1619 29.0071 7.65229 28.8176 8.10984C28.6281 8.56739 28.3503 8.98313 28.0001 9.33332L10.0001 27.3333L2.66675 29.3333L4.66675 22L22.6667 3.99999Z"
                  stroke="black"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
              </svg>
            </div>
          </div>
        </div>

        <div className="content">
          <div className="contentSections">
            <div className="contentSection1">
              <button className="notification">
                <svg
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M6 8C6 6.4087 6.63214 4.88258 7.75736 3.75736C8.88258 2.63214 10.4087 2 12 2C13.5913 2 15.1174 2.63214 16.2426 3.75736C17.3679 4.88258 18 6.4087 18 8C18 15 21 17 21 17H3C3 17 6 15 6 8Z"
                    fill="black"
                    stroke="black"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  />
                  <path
                    d="M8.9668 20.2115C9.26545 20.7548 9.7045 21.2078 10.2381 21.5233C10.7716 21.8389 11.3802 22.0053 12.0001 22.0053C12.6199 22.0053 13.2285 21.8389 13.762 21.5233C14.2956 21.2078 14.7347 20.7548 15.0333 20.2115"
                    stroke="black"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  />
                  <circle
                    cx="18.1461"
                    cy="2.85388"
                    r="2.85388"
                    fill="#FA1616"
                  />
                </svg>
              </button>
              <Link to={"/signUp"}>
                {userToDisplay ? (
                  <button
                    className="profileButton"
                    onClick={() => handleProfileButton()}
                  >
                    <svg
                      width="35"
                      height="37"
                      viewBox="0 0 35 37"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M23.2857 25.1509C23.2857 23.6732 22.6987 22.2561 21.6538 21.2113C20.609 20.1664 19.1919 19.5795 17.7142 19.5795M17.7142 19.5795C16.2366 19.5795 14.8195 20.1664 13.7747 21.2113C12.7298 22.2561 12.1428 23.6732 12.1428 25.1509M17.7142 19.5795C19.7656 19.5795 21.4285 17.9165 21.4285 15.8652C21.4285 13.8138 19.7656 12.1509 17.7142 12.1509C15.6629 12.1509 14 13.8138 14 15.8652C14 17.9165 15.6629 19.5795 17.7142 19.5795Z"
                        stroke="black"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                    </svg>

                    {userToDisplay.name}
                  </button>
                ) : (
                  <button
                    className="profileButton"
                    onClick={() => handleProfileButton()}
                  >
                    <svg
                      width="35"
                      height="37"
                      viewBox="0 0 35 37"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M23.2857 25.1509C23.2857 23.6732 22.6987 22.2561 21.6538 21.2113C20.609 20.1664 19.1919 19.5795 17.7142 19.5795M17.7142 19.5795C16.2366 19.5795 14.8195 20.1664 13.7747 21.2113C12.7298 22.2561 12.1428 23.6732 12.1428 25.1509M17.7142 19.5795C19.7656 19.5795 21.4285 17.9165 21.4285 15.8652C21.4285 13.8138 19.7656 12.1509 17.7142 12.1509C15.6629 12.1509 14 13.8138 14 15.8652C14 17.9165 15.6629 19.5795 17.7142 19.5795Z"
                        stroke="black"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                    </svg>
                    profile
                  </button>
                )}
              </Link>
            </div>
            <div className="contentSection2">
              <div className="contentSection2Writings">
                {userToDisplay ? (
                  <h3>Welcome {userToDisplay.name},</h3>
                ) : (
                  <h3>Welcome</h3>
                )}
                <p>Track your work time by creating a New Task.</p>
                {!task ? (
                  <button className="blaclButton" onClick={() => setTask(true)}>
                    <svg
                      width="13"
                      height="13"
                      viewBox="0 0 13 13"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M6.58301 0.666656V12.3333"
                        stroke="white"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                      <path
                        d="M0.749756 6.5H12.4164"
                        stroke="white"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                    </svg>
                    Task
                  </button>
                ) : (
                  <button
                    className="blaclButton"
                    onClick={() => setTask(false)}
                  >
                    <svg
                      width="14"
                      height="15"
                      viewBox="0 0 14 15"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M5.04994 7.50006L6.34994 8.80006L8.94994 6.20006M7 14C10.5899 14 13.5 11.0899 13.5 7.5C13.5 3.91005 10.5899 1 7 1C3.41005 1 0.5 3.91005 0.5 7.5C0.5 11.0899 3.41005 14 7 14Z"
                        stroke="white"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                    </svg>
                    Task
                  </button>
                )}
              </div>

              <img className="manImage" src="/Man.png" alt="man" />
            </div>
            <div className="contentSection3">
              <div className="contentSection3sub">
                <div className="contentSection3subWritings">
                  <h3>Create a new Project</h3>

                  <p>
                    Every task is associated with a specific project, Let’s
                    start a project now.
                    {project && (
                      <button className="whiteButtom">Next tip</button>
                    )}
                  </p>
                </div>
                {!project ? (
                  <button
                    className="blaclButton"
                    onClick={() => setProject(true)}
                  >
                    <svg
                      width="13"
                      height="13"
                      viewBox="0 0 13 13"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M6.58301 0.666656V12.3333"
                        stroke="white"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                      <path
                        d="M0.749756 6.5H12.4164"
                        stroke="white"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                    </svg>
                    Project
                  </button>
                ) : (
                  <button
                    className="blaclButton"
                    onClick={() => setProject(false)}
                  >
                    <svg
                      width="14"
                      height="15"
                      viewBox="0 0 14 15"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M5.04994 7.50006L6.34994 8.80006L8.94994 6.20006M7 14C10.5899 14 13.5 11.0899 13.5 7.5C13.5 3.91005 10.5899 1 7 1C3.41005 1 0.5 3.91005 0.5 7.5C0.5 11.0899 3.41005 14 7 14Z"
                        stroke="white"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                    </svg>
                    Project
                  </button>
                )}
              </div>

              <div className="contentSection3sub">
                <div className="contentSection3subWritings">
                  <h3>Add a Team</h3>

                  <p>
                    Every task is associated with a specific project, Let’s
                    start a project now.
                    {team && <button className="whiteButtom">Next tip</button>}
                  </p>
                </div>
                {!team ? (
                  <button className="blaclButton" onClick={() => setTeam(true)}>
                    <svg
                      width="13"
                      height="13"
                      viewBox="0 0 13 13"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M6.58301 0.666656V12.3333"
                        stroke="white"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                      <path
                        d="M0.749756 6.5H12.4164"
                        stroke="white"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                    </svg>
                    Team
                  </button>
                ) : (
                  <button
                    className="blaclButton"
                    onClick={() => setTeam(false)}
                  >
                    <svg
                      width="14"
                      height="15"
                      viewBox="0 0 14 15"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M5.04994 7.50006L6.34994 8.80006L8.94994 6.20006M7 14C10.5899 14 13.5 11.0899 13.5 7.5C13.5 3.91005 10.5899 1 7 1C3.41005 1 0.5 3.91005 0.5 7.5C0.5 11.0899 3.41005 14 7 14Z"
                        stroke="white"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                    </svg>
                    Team
                  </button>
                )}
              </div>

              <div className="contentSection3sub">
                <div className="contentSection3subWritings">
                  <h3>Finish your Performatrix</h3>

                  <p>Performatrix is a monthly performance score board.</p>
                  {start && <button className="whiteButtom">Next tip</button>}
                </div>
                {!start ? (
                  <button
                    className="blaclButton"
                    onClick={() => setStart(true)}
                  >
                    <svg
                      width="13"
                      height="13"
                      viewBox="0 0 13 13"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M6.58301 0.666656V12.3333"
                        stroke="white"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                      <path
                        d="M0.749756 6.5H12.4164"
                        stroke="white"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                    </svg>
                    Start
                  </button>
                ) : (
                  <button
                    className="blaclButton"
                    onClick={() => setStart(false)}
                  >
                    <svg
                      width="14"
                      height="15"
                      viewBox="0 0 14 15"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M5.04994 7.50006L6.34994 8.80006L8.94994 6.20006M7 14C10.5899 14 13.5 11.0899 13.5 7.5C13.5 3.91005 10.5899 1 7 1C3.41005 1 0.5 3.91005 0.5 7.5C0.5 11.0899 3.41005 14 7 14Z"
                        stroke="white"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                    </svg>
                    Start
                  </button>
                )}
              </div>

              <div className="contentSection3sub">
                <div className="contentSection3subWritings">
                  <h3>Change Theme</h3>

                  <p>You can switch themes from Light Mode into Dark mode.</p>
                  {!theme && <button className="whiteButtom">Next tip</button>}
                </div>
                {!theme ? (
                  <button
                    className="blaclButton"
                    onClick={() => setTheme(true)}
                  >
                    <svg
                      width="14"
                      height="15"
                      viewBox="0 0 14 15"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M5.04994 7.50006L6.34994 8.80006L8.94994 6.20006M7 14C10.5899 14 13.5 11.0899 13.5 7.5C13.5 3.91005 10.5899 1 7 1C3.41005 1 0.5 3.91005 0.5 7.5C0.5 11.0899 3.41005 14 7 14Z"
                        stroke="white"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                    </svg>
                    Dark Mode
                  </button>
                ) : (
                  <button
                    className="blaclButton"
                    onClick={() => setTheme(false)}
                  >
                    <svg
                      width="13"
                      height="13"
                      viewBox="0 0 13 13"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M6.58301 0.666656V12.3333"
                        stroke="white"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                      <path
                        d="M0.749756 6.5H12.4164"
                        stroke="white"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                    </svg>
                    Dark Mode
                  </button>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Body;
