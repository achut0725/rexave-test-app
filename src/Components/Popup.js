import React from "react";

function Popup({content}) {
  return (
    <>
      <div className="overlay">
        <div className="popup">
          <h3>{content}</h3>
        </div>
      </div>
    </>
  );
}

export default Popup;
