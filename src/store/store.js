import { configureStore } from "@reduxjs/toolkit";
import userSlice from "./UserSlice";
import LoginSlice from "./LoginSlice";

const store = configureStore({
  reducer: {
    user: userSlice,
    isLoggedIn: LoginSlice
  },
});

export default store;
