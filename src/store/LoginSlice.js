import { createSlice } from "@reduxjs/toolkit";

const loginSlice = createSlice({
    name: "isLoggedIn",
    initialState: {
        status : false,
    },
    reducers: {
        loggin: (state, action) => {
            state.status = action.payload;
        }
    }
})

export const { loggin } = loginSlice.actions;

export default loginSlice.reducer;